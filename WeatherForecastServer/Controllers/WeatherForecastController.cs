using Microsoft.AspNetCore.Mvc;

namespace WeatherForecastServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("GetWeatherForecast")]
        public async Task<object> GetForecastByCity(string cityName)
        {
            using var client = new HttpClient();
            using var response = await client.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={cityName}&appid=e203317f0df5474c05874e35b030eda3");
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }
    }
}